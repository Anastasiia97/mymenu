﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace whileMenu
{
    class Program
    {
        static void Main(string[] args)
        {
            bool displayMenu = true;
            while (displayMenu == true)
            {
                displayMenu = MainMenu();
            }
            
        }

        private static bool MainMenu()
        {
            Console.WriteLine("===>*************************************<===");
            Console.WriteLine("Menu: ");
            Console.WriteLine("1) only even or odd");
            Console.WriteLine("2) reverse");
            Console.WriteLine("3) game");
            Console.WriteLine("4) your age");
            Console.WriteLine("5) exit");
            string result = Console.ReadLine();
            if (result == "1")
            {
                Console.Clear();
                Console.WriteLine("Enter number to know even it or odd: ");
                int input = Convert.ToInt32(Console.ReadLine());
                OddEven(input);
                return true;

            }
            else if (result == "2")
            {
                Console.Clear();
                Console.WriteLine("Enter string to revers it: ");
                string input = Console.ReadLine();
                char[] reversInputChar = input.ToCharArray();
                Array.Reverse(reversInputChar);
                Console.WriteLine(reversInputChar);

                return true;

            }
            else if (result == "3")
            {
                Console.Clear();
                int counter = 1;
                bool incorrect = true;
                Random myRandom = new Random();
                int myRandomNum = myRandom.Next(1, 7);
                do
                {
                    Console.Write("Enter any number beetween 1 and 7: ");
                    int num = int.Parse(Console.ReadLine());
                    if (num == myRandomNum)
                    {
                        Console.WriteLine("Nice! {0} tries", counter);
                        incorrect = false;
                    }
                    else
                    {
                        Console.WriteLine("Try again!");
                    }
                    counter++;
                }
                while (incorrect);
                return true;
            }

            else if (result == "4")
            {
                Console.Clear();
                Age();
                return true;
            }

            else if (result == "5")
            {

                return false;
            }

            else
            {
                Console.Clear();
                Console.WriteLine("You enter is not correct");
                return true;
            }
                       
        }
        private static void OddEven(int num)
        {
            if (num%2 != 0)
            {
                Console.WriteLine("Num is odd digital: {0}", num);
                
            }
            else
            {
                Console.WriteLine("Num is even: {0}", num);
                
            }
        }


        private static void Age()
        {
            Console.Clear();
            Console.WriteLine("You will knew your age in days, housrs and e.g.");
            Console.WriteLine("Enter your birthday day (ex.  12/3/1998): ");
            DateTime birthday = DateTime.Parse(Console.ReadLine());
            TimeSpan yourAge = DateTime.Now.Subtract(birthday);
            
            Console.WriteLine("Days: {0}", yourAge.TotalDays);
            Console.WriteLine("Hours: {0}", yourAge.TotalHours);
            Console.WriteLine("Minutes: {0}", yourAge.TotalMinutes);
            Console.WriteLine("Seconds: {0}", yourAge.TotalSeconds);
            Console.WriteLine("TotalMilliseconds: {0}", yourAge.TotalMilliseconds);
        
            DateTime yourTen = birthday.AddDays(10000);
            Console.WriteLine("Your 10000 days: {0}", yourTen.ToLongDateString());
            DateTime yourTf = birthday.AddDays(15000);
            Console.WriteLine("Your 15000 days: {0}", yourTf.ToLongDateString());
            DateTime yourTwo = birthday.AddDays(20000);
            Console.WriteLine("Your 20000 days: {0}", yourTwo.ToLongDateString());
            DateTime yourThr = birthday.AddDays(30000);
            Console.WriteLine("Your 30000 days: {0}", yourThr.ToLongDateString());
            bool stop = true;
            do
            {
                Console.WriteLine("Enter any nums of days wich you want (ONLY INTEGER): ");
                int input = int.Parse(Console.ReadLine());
                DateTime yourInput = birthday.AddDays(input);
                Console.WriteLine("Your {1} days: {0}", yourInput.ToLongDateString(), Convert.ToString(input));
                Console.WriteLine("Want more? If yes enter - YES");
                string answer = Console.ReadLine();
                if (answer != "YES")
                {
                    stop = false;
                    Console.Clear();
                }
            }
            while (stop);

        }
        
    }
}
